package BT1_BaoNQ;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.bytebuddy.asm.Advice;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class QA_Tool {

    @Test
    public void QA_Tool() throws InterruptedException {

        WebDriver driver;
        Random rand;
        Select select;


        WebDriverManager.chromedriver().setup();
        System.out.println("Open Chrome");
        driver = new ChromeDriver();

        System.out.println("Open targetpage,");
        driver.get("https://demoqa.com/automation-practice-form");

        driver.manage().window().setSize(new Dimension(1936, 1048));


        driver.findElement(By.id("firstName")).click();
        driver.findElement(By.id("firstName")).sendKeys("Quoc Bao");

        driver.findElement(By.id("lastName")).click();
        driver.findElement(By.id("lastName")).sendKeys("Nguyen");

        rand = new Random();
        driver.findElement(By.id("userEmail")).click();

        try {
            String emailAddress = "baonq" + rand.nextInt(205354) + "@gmail.com";
            driver.findElement(By.id("userEmail")).sendKeys(emailAddress);
        } catch (Exception e) {
            System.out.println("That mail was existed!");
        }

        driver.findElement(By.cssSelector(".custom-radio:nth-child(1) > .custom-control-label")).click();

        driver.findElement(By.id("userNumber")).click();

        try {
            String sdt = "0522" + rand.nextInt(999999);
            driver.findElement(By.id("userNumber")).sendKeys(sdt);
        } catch (Exception e) {
            System.out.println("Phone number was existed!");
        }

        driver.findElement(By.id("dateOfBirthInput")).click();

        select = new Select(driver.findElement(By.cssSelector(".react-datepicker__year-select")));
        select.selectByVisibleText("2000");

        select = new Select(driver.findElement(By.cssSelector(".react-datepicker__month-select")));
        select.selectByVisibleText("August");
        Thread.sleep(1000);


        driver.findElement(By.cssSelector(".react-datepicker__day--001:nth-child(3)")).click();

        driver.findElement(By.id("subjectsInput")).sendKeys("English");
        driver.findElement(By.id("react-select-2-option-0")).click();

        driver.findElement(By.cssSelector(".custom-checkbox:nth-child(1) > .custom-control-label")).click();
        driver.findElement(By.cssSelector(".custom-checkbox:nth-child(2) > .custom-control-label")).click();
        driver.findElement(By.cssSelector(".custom-checkbox:nth-child(3) > .custom-control-label")).click();

        WebElement uploadImg = driver.findElement(By.id("uploadPicture"));
        String projectPath = System.getProperty("user.dir");
        uploadImg.sendKeys(projectPath + "/src/test/java/BT1_BaoNQ/avata-dep-nam-2.jpg");

        driver.findElement(By.id("currentAddress")).click();
        driver.findElement(By.id("currentAddress")).sendKeys("MWG");

        driver.findElement(By.id("react-select-3-input")).sendKeys("NCR");
        driver.findElement(By.id("react-select-3-option-0")).click();
        Thread.sleep(1000);

        driver.findElement(By.id("react-select-4-input")).sendKeys("Delhi");
        driver.findElement(By.id("react-select-4-option-0")).click();

        driver.findElement(By.id("submit")).submit();
        driver.findElement(By.id("closeLargeModal")).click();

    }
}
